import { Module, VuexAction, VuexModule } from 'nuxt-property-decorator'
import OperationType from '~/types/operation'

@Module({
  name: 'Operations',
  stateFactory: true,
  namespaced: true
})
export default class Operations extends VuexModule {
  operations: OperationType[] = [{
    id: 1,
    type: 'message',
    site: 'avito',
    number: 7634383426,
    message: 'Ваш код для регистрации на Авито. Никому его не говорите это... ',
    date: "05.09.2020 в 16:30:15."
  },
    {
      id: 2,
      type: 'message',
      site: 'avito',
      number: 7634383426,
      message: 'Ваш код для регистрации на Авито. Никому его не говорите это... ',
      date: "05.09.2020 в 16:30:15."
    },
    {
      id: 3,
      type: 'message',
      site: 'vk',
      number: 7634383426,
      message: 'Ваш код для регистрации на Авито. Никому его не говорите это... ',
      date: "05.09.2020 в 16:30:15."
    },
    {
      id: 4,
      type: 'message',
      site: 'facebook',
      number: 7634383426,
      message: 'Ваш код для регистрации на Авито. Никому его не говорите это... ',
      date: "05.09.2020 в 16:30:15."
    },
    {
      id: 5,
      type: 'other',
      site: 'avito',
      number: 7634383426,
      message: 'Ваш код для регистрации на Авито. Никому его не говорите это... ',
      date: "05.09.2020 в 16:30:15."
    },
    {
      id: 6,
      type: 'message',
      site: 'vk',
      number: 7634383426,
      message: 'Ваш код для регистрации на Авито. Никому его не говорите это... ',
      date: "05.09.2020 в 16:30:15."
    },
    {
      id: 7,
      type: 'other',
      site: 'vk',
      number: 7634383426,
      message: 'Ваш код для регистрации на Авито. Никому его не говорите это... ',
      date: "05.09.2020 в 16:30:15."
    },
    {
      id: 8,
      type: 'message',
      site: 'ok',
      number: 7634383426,
      message: 'Ваш код для регистрации на Авито. Никому его не говорите это... ',
      date: "05.09.2020 в 16:30:15."
    }
  ]

  @VuexAction
  getCollections (): any[] {
    return this.operations
  }
}
