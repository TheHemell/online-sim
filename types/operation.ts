export default interface OperationType {
  id: number,
  type: string,
  site: string,
  number: number,
  message?: string,
  date?: string,
}
